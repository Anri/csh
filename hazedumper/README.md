# Instructions

To update local files (in case online offset aren't up-to-date) :

1. Open CS:GO (wait for the menu screen)

2. Run the generation by double-clicking on `generate.bat`

3. The file has been generated, you can now use it with the `--offline` argument
