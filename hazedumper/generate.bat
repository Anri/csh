@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

IF NOT EXIST .\hazedumper.exe (
    ECHO Download latest release of hazedumper

    SET "dir=Windows"

    :: Download latest release
    FOR /f "tokens=1,* delims=:" %%A IN ('curl -ks ^
        https://api.github.com/repos/frk1/hazedumper-rs/releases/latest ^| ^
        find "browser_download_url"') DO (
        curl -kOL %%B
    )

    :: Unzip the file
    PowerShell -NoLogo -NoProfile -Command "& { $shell = New-Object -COM" ^
        "Shell.Application; $target = $shell.NameSpace('%CD%'); $zip =" ^
        "$shell.NameSpace('%CD%\!dir!.zip'); $target.CopyHere($zip.Items()," ^
        "16); }" "!dir!"

    :: Move the binary to the current directory
    MOVE .\!dir!\*.exe .\

    :: Delete unnecessary file and directory
    DEL !dir!.zip
    RMDIR !dir!\
)

IF NOT EXIST .\config.json (
    ECHO Download latest config file for CS:GO

    :: Download
    curl -kOL https://raw.githubusercontent.com/frk1/hazedumper/master/config.json
)

ECHO Generate offsets using hazedumper

:: Generate files
.\hazedumper.exe

EXIT /B
