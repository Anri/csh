from json import load, loads
from time import sleep

from pymem import Pymem
from requests import get


class Hack:
    """Base class for playing with CSGO memory"""

    def __init__(self, offline: bool = False) -> None:
        # Time values
        self.wait_time = 0.01
        self.timeout = self.wait_time * 50

        # Loading offsets
        self.offsets = self._find_offsets(offline)

        # Load virtual mapping of keys
        self.vmap = self._find_keys()

        # Load process
        self.pm = self._find_process(True)

    def _find_offsets(self, offline: bool) -> dict[str, int]:
        """Load CSGO offset from online repository or local file"""
        if offline:
            with open("hazedumper/csgo.min.json", "r") as f:
                serial_data = load(f)
        else:
            hazedumper_data = get(
                "https://raw.githubusercontent.com/frk1/hazedumper/master/csgo.min.json"
            )
            serial_data = loads(hazedumper_data.text)

        return (
            serial_data["signatures"]
            | serial_data["netvars"]
            | {
                "entity_size": 0x10,
                "glow_obj_size": 0x38,
                "glow_R": 0x8,
                "glow_G": 0xC,
                "glow_B": 0x10,
                "glow_A": 0x14,
                "GOM_wall": 0x27,
                "GOM_visible": 0x28,
                "render_R": 0x0,
                "render_G": 0x1,
                "render_B": 0x2,
                "float": 0x4,
                "head_idx": 0x30 * 8,
                "head_x": 0x0C,
                "head_y": 0x1C,
                "head_z": 0x2C,
                "player_info_items": 0x40,
                "player_info_item": 0xC,
                "player_info_elem": 0x28,
                "player_info_elem_size": 0x34,
            }
        )

    def _find_keys(self) -> dict[str, int]:
        """https://learn.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes"""
        return {
            "SPACE": 0x20,
            "+": 0xBB,
            "LBUTTON": 0x01,
            "END": 0x23,
            "PAGE_UP": 0x21,
            "PAGE_DOWN": 0x22,
        }

    def _find_process(self, verbose: bool = False) -> Pymem:
        """Find game process"""
        process_found = False
        print("Looking for process... ", end="", flush=True) if verbose else None

        pm = None
        while not process_found:
            try:
                pm = Pymem("csgo.exe")
            except:  # noqa: E722
                try:
                    sleep(self.timeout)
                except KeyboardInterrupt:
                    print("Canceled!")
                    exit(1)
            else:
                print("Process found!") if verbose else print("")
                process_found = True

        if pm:
            return pm
        exit(1)

    def find_module(self, module: str):
        """Find module address"""
        found = None
        for internal_module in list(self.pm.list_modules()):
            if internal_module.name == module + ".dll":
                found = internal_module.lpBaseOfDll

        if found:
            return found
        else:
            raise MemoryError(
                "Maybe the game isn't fully loaded yet? Wait for menu screen"
            )

    def find_uint(self, base, offset: int) -> int:
        """Find unsigned integer in memory for sure"""
        local_element = None
        while not local_element:
            local_element = self.pm.read_uint(base + offset)

            sleep(self.timeout)

        return int(local_element)

    def hack_loop(self, method, time: float | None = None):
        """Run the hack loop"""
        if time is None:
            time = self.wait_time

        while True:
            # Reduce CPU usage
            sleep(time)

            # Cheat
            method()
