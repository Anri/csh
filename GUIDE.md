# Easy steps to add a cheat to this app:

1. Create a file in [the cheat directory](./cheats/)
2. Create a class that inherits from `Hack`:

```python
from hack import Hack

class Mycheat(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        # Here you can initialize global variables for your cheat
```

3. Add the method who will contain the cheat logic, note that:
   - Your method must contain a sub-method and must be added to
     the `hackloop` at the end of the method (see example below)

```python
    def mycheat(self) -> None:
        # Cheat initialization, will only run once

        def cheat():
            # Your cheat logic, will be looped

        # Setting up the loop
        self.hack_loop(cheat)
```

4. If you need to unload something before closing the app (e.g. restore the
   initial state of something), you can add a second method called
   `MYCHEAT_unload` with `MYCHEAT` the same name as the previous method

```python
    def mycheat_unload(self) -> None:
        # Called when the app is closed
```

5. For more examples, see [the cheats already done](./cheats/)

6. If your cheat logic modifies the same memory of another cheat at the same
   time, then add that cheat to the list of incompatibilities in [cheat](./cheat.py)
   after the `Incompatible cheats` comment. Add only one entry for your cheat,
   it will automatically propagate to the other ones

7. Remember to add your cheat to the list in the [README](./README.md#about).

## Extra infos

- `self.pm` is used to read/write memory
- `self.offsets` contains a list of offsets
- `self.find_module` finds a module (`.dll`)
- `self.find_uint` always returns something (blocking thread)
- You can always add time between each loop call by calling `sleep`
- See the [utils](./utils.py) file if you need more tools, like a vector class,
  colour etc.
- If needed, feel free to add your own offsets to the `self.offsets` variable

# Miscellaneous

Please, use `black` before doing a pull request, as it's the formatter the
project use.

On Linux, you can copy the git hook stored in [githooks/](./githooks) who check
the code, preventing any commit with bad formatting.

```sh
$ cp githooks/* .git/hooks/
```
