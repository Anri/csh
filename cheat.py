from glob import glob

from hack import Hack

# Import all cheats
backslash = "\\"
for file in glob("cheats/*.py"):
    exec(f"from {file.replace(backslash, '.')[:-3]} import *")


class Cheat(*[child for child in Hack.__subclasses__()]):
    """All cheats are loaded in this class"""

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

        self.cheats_list = [
            func
            for func in dir(self)
            # Function
            if callable(getattr(self, func))
            # User defined
            if not func.startswith("_")
            # Hack loop
            if not func == "hack_loop"
            # Unloaders
            if not func.endswith("_unload")
            # Utils
            if not func.startswith("find_")
        ]

        self.incompatible = dict.fromkeys(self.cheats_list, [])

        # Incompatible cheats
        self.incompatible["aimbot"] = ["norecoil"]

        # Builds the implicit list
        for cheat, incompatible_cheat_list in self.incompatible.items():
            for incompatible_cheat in incompatible_cheat_list:
                if cheat not in self.incompatible[incompatible_cheat]:
                    # Propagate implicit cheat incompatibilities
                    self.incompatible[incompatible_cheat] = self.incompatible[
                        incompatible_cheat
                    ] + [cheat]
