# CSGOHACKS

External cheats for Windows, educational content only.

## How to use?

1. [Download the `.zip`](https://git.mylloon.fr/Anri/csh/archive/main.zip)

2. Run it by double-clicking on `run.bat`

### Extra info

- You can use a local file for offsets (see [this documentation](./hazedumper)
  to generate the `.json` file) by passing `--offline` to the app:

```powershell
python .\main.py --offline
```

- You also can bypass the interactive cheat selector by passing a list to the app.

For instance, using `aimbot`, `bhop` and the `radar hack`

```powershell
python .\main.py --list=aimbot,bhop,radarhack
```

Feel free to modify the [run.bat](./run.bat) as you wish.

## How to not use?

**CTRL + C** (twice for the .bat script) will stop the app.

> Do not close the window, **CTRL + C** will unload some cheats, which will
> restore the initial state after closing.

## About

- Aim bot
- Bhop
- Chams (ennemy different color and more visible)
- FOV
- Glow (wallhack)
- Money reveal
- No-flash
- No-recoil
- Radar hack
- Trigger hack

Want to add a cheat? See [the guide](./GUIDE.md)!
