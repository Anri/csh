from win32api import GetAsyncKeyState

from hack import Hack
from utils import Vec, angle_normalizer, hypot


class Aimbot(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

        # AimBot FOV, higher = more aggressive
        self.__fov_range = 4.0

        # Smoothing, more = less efficient
        self.__smoothing = 6.0

    def aimbot(self) -> None:
        # Aliases
        mem = self.pm
        offset = self.offsets

        # Get module addresses
        client = self.find_module("client")
        engine = self.find_module("engine")

        # Get local player
        local_player = self.find_uint(client, offset["dwLocalPlayer"])

        # Get client state
        client_state = self.find_uint(engine, offset["dwClientState"])

        def cheat():
            # Pressing left click
            if not GetAsyncKeyState(self.vmap["LBUTTON"]):
                return

            # Where are the eyes
            local_eye_pos = Vec(
                float(mem.read_float(local_player + offset["m_vecOrigin"])),
                float(
                    mem.read_float(
                        local_player + offset["m_vecOrigin"] + offset["float"]
                    )
                ),
                float(
                    mem.read_float(
                        local_player + offset["m_vecOrigin"] + offset["float"] * 2
                    )
                ),
            ).plus(
                Vec(
                    float(mem.read_float(local_player + offset["m_vecViewOffset"])),
                    float(
                        mem.read_float(
                            local_player + offset["m_vecViewOffset"] + offset["float"]
                        )
                    ),
                    float(
                        mem.read_float(
                            local_player
                            + offset["m_vecViewOffset"]
                            + offset["float"] * 2
                        )
                    ),
                )
            )

            # Where player is looking
            view_angles = Vec(
                float(
                    mem.read_float(client_state + offset["dwClientState_ViewAngles"])
                ),
                float(
                    mem.read_float(
                        client_state
                        + offset["dwClientState_ViewAngles"]
                        + offset["float"]
                    )
                ),
            )

            # How much the view is modified
            aim_punch = Vec(
                float(mem.read_float(local_player + offset["m_aimPunchAngle"])),
                float(
                    mem.read_float(
                        local_player + offset["m_aimPunchAngle"] + offset["float"]
                    )
                ),
            ).times(
                2.0
            )  # server multiple punch by 2

            # Will hold info about the closest ennemy
            best_distance = self.__fov_range
            best_angle = Vec()

            # Loop all entities
            for i in range(1, 32):  # 0 is world
                entity = int(
                    mem.read_uint(
                        client + offset["dwEntityList"] + i * offset["entity_size"]
                    )
                )

                # Ignore if entity doesn't exist
                if not entity:
                    continue

                # Ignore allies
                if mem.read_int(entity + offset["m_iTeamNum"]) == (
                    mem.read_int(local_player + offset["m_iTeamNum"])
                ):
                    continue

                # Ignore dormant
                if mem.read_bool(entity + offset["m_bDormant"]):
                    continue

                # Check if ennemy is alive
                if mem.read_int(entity + offset["m_lifeState"]):
                    continue

                # Don't aim at ennemy behind a wall
                if not (
                    int(mem.read_int(entity + offset["m_bSpottedByMask"]))
                    & (
                        1
                        << int(
                            mem.read_int(
                                client_state + offset["dwClientState_GetLocalPlayer"]
                            )
                        )
                    )
                ):
                    continue

                # Find head
                boneMatrix = int(mem.read_uint(entity + offset["m_dwBoneMatrix"]))
                ennemy_head = Vec(
                    float(
                        mem.read_float(
                            boneMatrix + offset["head_idx"] + offset["head_x"]
                        )
                    ),
                    float(
                        mem.read_float(
                            boneMatrix + offset["head_idx"] + offset["head_y"]
                        )
                    ),
                    float(
                        mem.read_float(
                            boneMatrix + offset["head_idx"] + offset["head_z"]
                        )
                    ),
                )

                # Angle to ennemy's head
                angle = calculate_angle(
                    local_eye_pos, ennemy_head, view_angles.plus(aim_punch)
                )

                # Current FOV
                distance = hypot(angle.x, angle.y)

                # If an ennemy is close
                if distance < best_distance:
                    best_distance = distance
                    best_angle = angle

            # We found an ennemy to aim at
            if not best_angle.is_zero():
                # Apply the smoothing if needed
                if self.__smoothing > 0.0:
                    best_angle = best_angle.div(self.__smoothing)

                # Angle from player to ennemy's head
                final_angle = view_angles.plus(best_angle)

                # Fix angle if needed
                final_angle = angle_normalizer(final_angle)

                # Move player's view angle
                mem.write_float(
                    client_state + offset["dwClientState_ViewAngles"], final_angle.x
                )
                mem.write_float(
                    client_state + offset["dwClientState_ViewAngles"] + offset["float"],
                    final_angle.y,
                )

        self.hack_loop(cheat)


def calculate_angle(local_pos: Vec, ennemy_pos: Vec, angles: Vec) -> Vec:
    """Angle calculation for aimbot"""
    return ennemy_pos.minus(local_pos).to_angle().minus(angles)
