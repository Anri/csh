from hack import Hack
from utils import Color4f


class Glow(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

        self.__color = Color4f(1.0, 0.0, 0.0, 0.8)

    def glow(self) -> None:
        # Aliases
        mem = self.pm
        offset = self.offsets

        # Get module address
        client = self.find_module("client")

        # Get local player
        local_player = self.find_uint(client, offset["dwLocalPlayer"])

        # Get glow object manager
        glow_obj_manager = self.find_uint(client, offset["dwGlowObjectManager"])

        def cheat():
            # Loop all entities
            for i in range(1, 32):  # 0 is world
                entity = int(
                    mem.read_uint(
                        client + offset["dwEntityList"] + i * offset["entity_size"]
                    )
                )

                # Ignore if entity doesn't exist
                if not entity:
                    continue

                # Ignore allies
                if mem.read_int(entity + offset["m_iTeamNum"]) == mem.read_int(
                    local_player + offset["m_iTeamNum"]
                ):
                    continue

                # Ignore dormant
                if mem.read_bool(entity + offset["m_bDormant"]):
                    continue

                # Check if ennemy is alive
                if mem.read_int(entity + offset["m_lifeState"]):
                    continue

                # Space between values
                i = (
                    int(mem.read_int(entity + offset["m_iGlowIndex"]))
                    * offset["glow_obj_size"]
                )

                # Change color glow
                if self.__color.r > 0.0:
                    mem.write_float(
                        glow_obj_manager + i + offset["glow_R"], self.__color.r
                    )
                if self.__color.g > 0.0:
                    mem.write_float(
                        glow_obj_manager + i + offset["glow_G"], self.__color.g
                    )
                if self.__color.b > 0.0:
                    mem.write_float(
                        glow_obj_manager + i + offset["glow_B"], self.__color.b
                    )
                if self.__color.a > 0.0:
                    mem.write_float(
                        glow_obj_manager + i + offset["glow_A"], self.__color.a
                    )

                # Render when not visible
                mem.write_bool(glow_obj_manager + i + offset["GOM_wall"], True)

                # Render when visible
                mem.write_bool(glow_obj_manager + i + offset["GOM_visible"], True)

        self.hack_loop(cheat)
