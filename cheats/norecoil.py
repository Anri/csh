from hack import Hack
from utils import Vec, angle_normalizer


class Norecoil(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

        # Control variable
        self.__old_punch = Vec()

    def norecoil(self) -> None:
        # Aliases
        mem = self.pm
        offset = self.offsets

        # Get module addresses
        client = self.find_module("client")
        engine = self.find_module("engine")

        # Get local player
        local_player = self.find_uint(client, offset["dwLocalPlayer"])

        # Get client state
        client_state = int(mem.read_uint(engine + offset["dwClientState"]))

        def cheat():
            # Check if player is shooting
            if mem.read_int(local_player + offset["m_iShotsFired"]):
                # Where player is looking
                view_angles = Vec(
                    float(
                        mem.read_float(
                            client_state + offset["dwClientState_ViewAngles"]
                        )
                    ),
                    float(
                        mem.read_float(
                            client_state
                            + offset["dwClientState_ViewAngles"]
                            + offset["float"]
                        )
                    ),
                )

                # Bullet shift
                aim_punch = Vec(
                    float(mem.read_float(local_player + offset["m_aimPunchAngle"])),
                    float(
                        mem.read_float(
                            local_player + offset["m_aimPunchAngle"] + offset["float"]
                        )
                    ),
                ).times(
                    2.0
                )  # server multiple punch by 2

                # New angles
                new_angle = angle_normalizer(
                    view_angles.plus(self.__old_punch).minus(aim_punch)
                )

                # Cancel recoil by moving the player view angle
                mem.write_float(
                    client_state + offset["dwClientState_ViewAngles"], new_angle.x
                )
                mem.write_float(
                    client_state + offset["dwClientState_ViewAngles"] + offset["float"],
                    new_angle.y,
                )

                # Save the current spray value for the next bullet
                self.__old_punch = aim_punch

            else:
                # Not spraying
                self.__old_punch = Vec()

        self.hack_loop(cheat)
