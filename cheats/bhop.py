from win32api import GetAsyncKeyState

from hack import Hack, sleep


class Bhop(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def bhop(self) -> None:
        # Aliases
        mem = self.pm
        offset = self.offsets

        # Get client
        client = self.find_module("client")

        # Get player
        local_player = self.find_uint(client, offset["dwLocalPlayer"])

        def cheat():
            # Pressing space bar
            if not GetAsyncKeyState(self.vmap["SPACE"]):
                return

            # Check if player is alive
            if mem.read_uint(local_player + offset["m_lifeState"]):
                return

            # Check if player on ground
            if int(mem.read_uint(local_player + offset["m_fFlags"])) & (1 << 0):
                mem.write_uint(client + offset["dwForceJump"], 5)
                sleep(0.01)
                mem.write_uint(client + offset["dwForceJump"], 4)

        self.hack_loop(cheat)
