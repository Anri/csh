from hack import Hack, sleep


class Rankreveal(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

        self.ranks = [
            "Unranked",
            "Silver I",
            "Silver II",
            "Silver III",
            "Silver IV",
            "Silver Elite (V)",
            "Silver Elite Master (VI)",
            "Gold Nova I",
            "Gold Nova II",
            "Gold Nova III",
            "Gold Nova Master (IV)",
            "Master Guardian I",
            "Master Guardian II",
            "Master Guardian Elite (III)",
            "Distinguished Master Guardian (IV)",
            "Legendary Eagle (I)",
            "Legendary Eagle Master (II)",
            "Supreme Master First Class (III)",
            "The Global Elite",
        ]

    def _rankreveal(self) -> None:
        # Aliases
        mem = self.pm
        offset = self.offsets

        # Get module addresses
        client = self.find_module("client")
        engine = self.find_module("engine")

        # Get client state
        client_state = self.find_uint(engine, offset["dwClientState"])

        def cheat():
            founds = []
            # Loop all entities
            for i in range(1, 32):  # 0 is world
                entity = int(
                    mem.read_uint(
                        client + offset["dwEntityList"] + i * offset["entity_size"]
                    )
                )

                # Ignore if entity doesn't exist
                if not entity:
                    continue

                # Ignore non-player
                if not mem.read_uint(entity + offset["m_iTeamNum"]):
                    continue

                # Get player info
                player_info = int(
                    mem.read_uint(client_state + offset["dwClientState_PlayerInfo"])
                )

                # Get player's item
                player_info_items = int(
                    mem.read_uint(
                        int(mem.read_uint(player_info + offset["player_info_items"]))
                        + offset["player_info_item"],
                    )
                )

                info = int(
                    mem.read_uint(
                        player_info_items
                        + offset["player_info_elem"]
                        + (i * offset["player_info_elem_size"])
                    )
                )

                name_entity = mem.read_string(info + offset["entity_size"])
                if name_entity != "GOTV":
                    player_ressources = int(
                        mem.read_uint(client + offset["dwPlayerResource"])
                    )

                    rank = int(
                        mem.read_uint(
                            player_ressources
                            + offset["m_iCompetitiveRanking"]
                            + (i * 4)
                        )
                    )

                    founds.append((name_entity, rank))

            if len(founds) > 0:
                # Print ranks
                max_len = len(max(list(zip(*founds))[0], key=len))  # use for formatting
                [print(f"{i:{max_len}} --> {self.ranks[j]}") for (i, j) in founds]
                print("")
                founds = []

                sleep(1000)
            else:
                sleep(2)

        self.hack_loop(cheat)
