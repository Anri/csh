from hack import Hack, sleep
from utils import errors_unload


class Noflash(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

        self.__brightness = 30.0
        self.__default_brightness = 255.0

    def noflash(self) -> None:
        # Aliases
        mem = self.pm
        offset = self.offsets

        # Get module address
        client = self.find_module("client")

        # Get local player
        self.__local_player = self.find_uint(client, offset["dwLocalPlayer"])
        local_player = self.__local_player

        def cheat():
            # Override the brightness value
            mem.write_float(
                local_player + offset["m_flFlashMaxAlpha"], self.__brightness
            )

            # Value only overrided on first flash or when server fullupdate
            sleep(10)

        self.hack_loop(cheat)

    def noflash_unload(self):
        # Aliases
        mem = self.pm
        offset = self.offsets
        try:
            local_player = self.__local_player
        except errors_unload():
            # No modification has been done
            return

        # Reset to default value
        mem.write_float(
            local_player + offset["m_flFlashMaxAlpha"], self.__default_brightness
        )
