from win32api import GetAsyncKeyState

from hack import Hack
from utils import errors_unload


class Fov(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

        self.__fov = None
        self.__factor = 5
        self.__default_fov = 90

    def fov(self) -> None:
        # Aliases
        mem = self.pm
        offset = self.offsets

        # Get module addresses
        client = self.find_module("client")

        # Get local player
        self.__local_player = self.find_uint(client, offset["dwLocalPlayer"])
        local_player = self.__local_player

        # Get FOV
        self.__fov = self.find_uint(local_player, offset["m_iDefaultFOV"])

        def cheat():
            if self.__fov is None:
                raise ValueError("FOV isn't defined.")
            fov = self.__fov

            # Reset FOV
            if GetAsyncKeyState(self.vmap["END"]):
                self.__fov = 90

            # Increase FOV
            elif GetAsyncKeyState(self.vmap["PAGE_DOWN"]) and self.__fov < 130:
                self.__fov += self.__factor

            # Decrease FOV
            elif GetAsyncKeyState(self.vmap["PAGE_UP"]) and self.__fov > 50:
                self.__fov -= self.__factor

            # If modified
            if fov != self.__fov:
                # Override the FOV value
                mem.write_int(local_player + offset["m_iDefaultFOV"], self.__fov)

        self.hack_loop(cheat)

    def fov_unload(self):
        # Aliases
        mem = self.pm
        offset = self.offsets
        try:
            local_player = self.__local_player
        except errors_unload():
            # No modification has been done
            return

        # Reset to default value if changed
        if self.__fov not in [None, self.__default_fov]:
            mem.write_int(local_player + offset["m_iDefaultFOV"], self.__default_fov)
