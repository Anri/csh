from hack import Hack
from re import search


class Moneyreveal(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

        self.__adress = self.__get_address(self.pm)
        self.__default = 0x75

    def __get_address(self, mem) -> int:
        client = 0
        clientModule = b""
        for internal_module in list(mem.list_modules()):
            if internal_module.name == "client.dll":
                client = internal_module.lpBaseOfDll
                clientModule = mem.read_bytes(client, internal_module.SizeOfImage)
                break

        offset = search(rb".\x0C\x5B\x5F\xB8\xFB\xFF\xFF\xFF", clientModule)
        if offset is not None:
            return client + offset.start()
        else:
            raise Exception("Offset not found for Money Reveal")

    def moneyreveal(self) -> None:
        """
        For this one, we're not using regular self.find_module and offsets
        methods because we regexing client.dll for a specific chunk of memory.
        Once we have what we want, we can change values.
        Moreover, change persists across game, so we only write once.
        """
        # Get module address
        self.pm.write_uchar(self.__adress, 0xEB)

    def moneyreveal_unload(self):
        # Reset to default value
        self.pm.write_uchar(self.__adress, self.__default)
