from struct import pack

from hack import Hack
from utils import Color3i, errors_unload


class Chams(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

        self.__color = (Color3i(255, 255, 0), 5.0)
        self.__default_color = None

    def chams(self) -> None:
        # Aliases
        mem = self.pm
        offset = self.offsets
        color = self.__color[0]
        brightness = self.__color[1]

        # Get module addresses
        self.__client = self.find_module("client")
        self.__engine = self.find_module("engine")
        client = self.__client
        engine = self.__engine

        # Get local player
        local_player = self.find_uint(client, offset["dwLocalPlayer"])

        def cheat():
            # Loop all entities
            for i in range(1, 32):  # 0 is world
                entity = int(
                    mem.read_uint(
                        client + offset["dwEntityList"] + i * offset["entity_size"]
                    )
                )

                # Ignore if entity doesn't exist
                if not entity:
                    continue

                # Ignore allies
                if mem.read_int(entity + offset["m_iTeamNum"]) == mem.read_int(
                    local_player + offset["m_iTeamNum"]
                ):
                    continue

                # Space between values
                i = (
                    int(mem.read_int(entity + offset["m_iGlowIndex"]))
                    * offset["glow_obj_size"]
                )

                if not self.__default_color:
                    self.__default_color = (
                        Color3i(
                            int(
                                mem.read_uint(
                                    entity + offset["m_clrRender"] + offset["render_R"]
                                )
                            ),
                            int(
                                mem.read_uint(
                                    entity + offset["m_clrRender"] + offset["render_G"]
                                )
                            ),
                            int(
                                mem.read_uint(
                                    entity + offset["m_clrRender"] + offset["render_B"]
                                )
                            ),
                        ),
                        mem.read_int(engine + offset["model_ambient_min"]),
                    )

                # Change color
                if color.r > 0:
                    mem.write_uint(
                        entity + offset["m_clrRender"] + offset["render_R"], color.r
                    )
                if color.g > 0:
                    mem.write_uint(
                        entity + offset["m_clrRender"] + offset["render_G"], color.g
                    )
                if color.b > 0:
                    mem.write_uint(
                        entity + offset["m_clrRender"] + offset["render_B"], color.b
                    )

                # Override the brightness of models by increasing
                # the minimal brightness value
                mem.write_int(
                    engine + offset["model_ambient_min"],
                    int.from_bytes(pack("f", brightness), byteorder="little")
                    ^ (engine + offset["model_ambient_min"] - 0x2C),
                )

        self.hack_loop(cheat)

    def chams_unload(self):
        # Aliases
        mem = self.pm
        offset = self.offsets
        try:
            color = self.__default_color[0]  # type: ignore
            brightness = self.__default_color[1]  # type: ignore
            client = self.__client
            engine = self.__engine
        except errors_unload():
            # No modification has been done
            return

        # Loop all entities
        for i in range(1, 32):  # 0 is world
            entity = int(
                mem.read_uint(
                    client + offset["dwEntityList"] + i * offset["entity_size"]
                )
            )

            # Ignore if entity doesn't exist
            if not entity:
                continue

            # Reset to default value if needed
            if color.r > 0:
                mem.write_uint(
                    entity + offset["m_clrRender"] + offset["render_R"], color.r
                )
            if color.g > 0:
                mem.write_uint(
                    entity + offset["m_clrRender"] + offset["render_G"], color.g
                )
            if color.b > 0:
                mem.write_uint(
                    entity + offset["m_clrRender"] + offset["render_B"], color.b
                )

            mem.write_int(engine + offset["model_ambient_min"], brightness)
