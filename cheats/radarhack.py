from hack import Hack


class Radarhack(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def radarhack(self) -> None:
        # Aliases
        mem = self.pm
        offset = self.offsets

        # Get module address
        client = self.find_module("client")

        # Get local player
        local_player = self.find_uint(client, offset["dwLocalPlayer"])

        def cheat():
            # Loop all entities
            for i in range(1, 32):  # 0 is world
                entity = int(
                    mem.read_uint(
                        client + offset["dwEntityList"] + i * offset["entity_size"]
                    )
                )

                # Ignore if entity doesn't exist
                if not entity:
                    continue

                # Ignore allies
                if mem.read_int(entity + offset["m_iTeamNum"]) == mem.read_int(
                    local_player + offset["m_iTeamNum"]
                ):
                    continue

                # Ignore dormant
                if mem.read_bool(entity + offset["m_bDormant"]):
                    continue

                # Check if ennemy is alive
                if mem.read_int(entity + offset["m_lifeState"]):
                    continue

                # Mark ennemy as spotted
                mem.write_bool(entity + offset["m_bSpotted"], True)

        self.hack_loop(cheat)
