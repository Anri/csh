from win32api import GetAsyncKeyState

from hack import Hack, sleep


class Trigger(Hack):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def trigger(self) -> None:
        # Aliases
        mem = self.pm
        offset = self.offsets

        # Get module address
        client = self.find_module("client")

        # Get local player
        local_player = self.find_uint(client, offset["dwLocalPlayer"])

        def cheat():
            # Pressing trigger key
            if not GetAsyncKeyState(self.vmap["+"]):
                return

            # Check if player is alive
            if mem.read_int(local_player + offset["m_lifeState"]):
                return

            # Get crosshair info about what we aiming at
            crosshair_id = int(mem.read_int(local_player + offset["m_iCrosshairId"]))

            # 0 is wall, +64 isn't a player
            if (crosshair_id == 0) or (crosshair_id > 64):
                return

            # Get ennemy under crosshair
            ennemy = int(
                mem.read_uint(
                    client
                    + offset["dwEntityList"]
                    + (crosshair_id - 1) * offset["entity_size"]
                )
            )

            # Ignore dormant
            if mem.read_bool(ennemy + offset["m_bDormant"]):
                return

            # Check if ennemy is alive
            if mem.read_int(ennemy + offset["m_lifeState"]):
                return

            # Ignore allies
            if mem.read_int(ennemy + offset["m_iTeamNum"]) == mem.read_int(
                local_player + offset["m_iTeamNum"]
            ):
                return

            # Shoot
            mem.write_uint(client + offset["dwForceAttack"], 6)
            sleep(0.2)
            mem.write_uint(client + offset["dwForceAttack"], 4)

        self.hack_loop(cheat)
